#include "zbarvideofilter.h"
#include "zbar.h"
#include <QDebug>

QVideoFilterRunnable *ZBarVideoFilter::createFilterRunnable()
{
    return new ZBarFilterRunnable(this);
}

ZBarFilterRunnable::ZBarFilterRunnable(ZBarVideoFilter *filter) :
    m_filter(filter)
{
}

ZBarFilterRunnable::~ZBarFilterRunnable()
{

}

QVideoFrame ZBarFilterRunnable::run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, QVideoFilterRunnable::RunFlags flags)
{
    zbar::ImageScanner zis;
    zis.set_config(zbar::ZBAR_QRCODE, zbar::ZBAR_CFG_ENABLE, 1);

    int width = input->width();
    int height= input->height();

    input->map(QAbstractVideoBuffer::ReadOnly);
    unsigned char * data = input->bits();

    if(data)
    {
        zbar::Image image(width, height, "Y800" , data, width*height);
        input->unmap();
    // Image image(width, height, "GRAY" , data, width*height);

    // scan the image for barcodes
    int n = zis.scan(image);


    // extract results
    for(zbar::Image::SymbolIterator symbol = image.symbol_begin();
        symbol != image.symbol_end();
        ++symbol)
    {
        // do something useful with results
        qDebug() << "decoded " << QString::fromStdString(symbol->get_type_name())
                 << " symbol \"" << QString::fromStdString(symbol->get_data()) << '"' << endl;
        //       m_qString = QString::fromStdString(symbol->get_data());

        //       emit m_filter->finished(QString::fromStdString(symbol->get_data()));
        return *input;
    }
    // clean up
    image.set_data(NULL, 0);
    }
    return *input;
}
