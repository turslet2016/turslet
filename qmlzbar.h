#ifndef QMLZBAR_H
#define QMLZBAR_H

#include <QObject>
#include <QQuickItem>
#include <QImage>
#include <QAbstractVideoFilter>
#include <QVideoFilterRunnable>
#include "zbar.h"

class QmlZBarRunnable : public QVideoFilterRunnable
{
    Q_OBJECT
    Q_PROPERTY(QImage source MEMBER m_qImage)
    Q_PROPERTY(QString text READ text NOTIFY decoded)

    QString m_qString;
    QImage m_qImage;
public:
    explicit QmlZBarRunnable(QObject *parent = 0);

    Q_INVOKABLE QString text(){
        return m_qString;
    }
    QVideoFrame run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags);
signals:
    void decoded();

public slots:
    void test();

};

class QmlZBar : public QAbstractVideoFilter {
    Q_OBJECT
public:
    QVideoFilterRunnable *createFilterRunnable() { return new QmlZBarRunnable; }
signals:
    void finished(QObject *result);
};


#endif // QMLZBAR_H
