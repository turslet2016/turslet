#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include "zbarvideofilter.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    qmlRegisterType<ZBarVideoFilter>("com.test", 1, 0, "ZBarFilter");
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    return app.exec();
}
