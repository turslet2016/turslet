#include "qmlzbar.h"
#include <QImage>


void QmlZBarRunnable::test()
{
    zbar::ImageScanner zis;
    zis.set_config(zbar::ZBAR_QRCODE, zbar::ZBAR_CFG_ENABLE, 1);

//    if(m_qImage.isNull())
//        m_qImage.load("test.jpg");


    QImage convertedImage = m_qImage.convertToFormat(QImage::Format_Grayscale8,Qt::MonoOnly);

    int width = convertedImage.width();
       int height= convertedImage.height();


       unsigned char * data = convertedImage.bits();

       zbar::Image image(width, height, "Y800" , data, width*height);
     // Image image(width, height, "GRAY" , data, width*height);

    // scan the image for barcodes
    int n = zis.scan(image);


    // extract results
    for(zbar::Image::SymbolIterator symbol = image.symbol_begin();
        symbol != image.symbol_end();
        ++symbol)
    {
        // do something useful with results
       qDebug() << "decoded " << QString::fromStdString(symbol->get_type_name())
             << " symbol \"" << QString::fromStdString(symbol->get_data()) << '"' << endl;
       m_qString = QString::fromStdString(symbol->get_data());
       emit decoded();
    }
    qDebug()<<"fsdfs";
    // clean up
    image.set_data(NULL, 0);
}
