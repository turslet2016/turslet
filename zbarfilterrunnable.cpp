#include "zbarfilter.h"
#include "zbarfilterrunnable.h"
#include <QDebug>

ZBarFilterRunnable::ZBarFilterRunnable(QAbstractVideoFilter *filter): QVideoFilterRunnable(){
//    m_filter = filter;
     zis.set_config(zbar::ZBAR_QRCODE, zbar::ZBAR_CFG_ENABLE, 1);
}

QVideoFrame ZBarFilterRunnable::run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, QVideoFilterRunnable::RunFlags flags)
{


    int width = input->width();
       int height= input->height();


       unsigned char * data = input->bits();

       zbar::Image image(width, height, "Y800" , data, width*height);
     // Image image(width, height, "GRAY" , data, width*height);

    // scan the image for barcodes
    int n = zis.scan(image);


    // extract results
    for(zbar::Image::SymbolIterator symbol = image.symbol_begin();
        symbol != image.symbol_end();
        ++symbol)
    {
        // do something useful with results
       qDebug() << "decoded " << QString::fromStdString(symbol->get_type_name())
             << " symbol \"" << QString::fromStdString(symbol->get_data()) << '"' << endl;
//       m_qString = QString::fromStdString(symbol->get_data());

//       emit m_filter->finished(QString::fromStdString(symbol->get_data()));
       return *input;
    }
    qDebug()<<"fsdfs";
    // clean up
    image.set_data(NULL, 0);
}
