QT += qml quick multimedia

CONFIG += c++11

SOURCES += main.cpp \
    zbarvideofilter.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)


HEADERS += \
    zbarvideofilter.h

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android


unix:android: LIBS += -L$$PWD/../zbar-arm/src/zbar-0.10/123/lib/ -lzbar -liconv
unix:!android: LIBS += -L$$PWD/../zbar/src/zbar-0.10/123/lib/ -lzbar -liconv

INCLUDEPATH += $$PWD/../zbar-arm/src/zbar-0.10/123/include
DEPENDPATH += $$PWD/../zbar-arm/src/zbar-0.10/123/include


unix:android: PRE_TARGETDEPS += $$PWD/../zbar-arm/src/zbar-0.10/123/lib/libzbar.a
unix:android: PRE_TARGETDEPS += $$PWD/../zbar-arm/src/zbar-0.10/123/lib/libiconv.a
unix:!android: PRE_TARGETDEPS += $$PWD/../zbar/src/zbar-0.10/123/lib/libzbar.a
