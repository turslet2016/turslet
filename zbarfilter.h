#ifndef ZBARFILTER_H
#define ZBARFILTER_H

#include "zbarfilterrunnable.h"

#include <QVideoFilterRunnable>


class ZBarFilter : public QAbstractVideoFilter
{
public:
//    ZBarFilter() {}
    QVideoFilterRunnable *createFilterRunnable() { return new ZBarFilterRunnable(this); }
signals:
    void finished(QString result);
};

#endif // ZBARFILTER_H
