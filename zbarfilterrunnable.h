#ifndef ZBARFILTERRUNNABLE_H
#define ZBARFILTERRUNNABLE_H


#include <QVideoFilterRunnable>
#include "zbar.h"
#include "zbarfilter.h"


class ZBarFilterRunnable : public QVideoFilterRunnable
{
    zbar::ImageScanner zis;
//    ZBarFilter *m_filter;
public:
    explicit ZBarFilterRunnable(QAbstractVideoFilter *filter);

    // QVideoFilterRunnable interface
public:
    QVideoFrame run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags);
};

#endif // ZBARFILTERRUNNABLE_H
