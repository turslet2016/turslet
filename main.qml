import QtQuick 2.6
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0

import com.test 1.0
import QtMultimedia 5.5

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

//    QZBar{
//        id:qz
//        onFinished: {
//            console.log('decoded '+qz.text)
//            label.text = qz.text
//        }
//    }
    ZBarFilter{
        id:zbar
//        onFinished: console.log("results of the computation: " + result)
    }

    Camera{
        id:camera
        onError: {
            console.log(errorCode+errorString)
        }
    }
    
    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page1 {
            Image {
                id: image1
                source: "test.jpg"
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        qz.test()
                    }
                }
            }
        }

        Page {
            
            VideoOutput {
                id: videoOutput
                source: camera
                anchors.fill: parent
                filters: zbar
            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("First")
        }
        TabButton {
            text: qsTr("Second")
        }
    }
}
