#ifndef ZBARVIDEOFILTER_H
#define ZBARVIDEOFILTER_H

#include <QAbstractVideoFilter>

class ZBarVideoFilter : public QAbstractVideoFilter
{
    Q_OBJECT

public:
    QVideoFilterRunnable *createFilterRunnable() Q_DECL_OVERRIDE;

signals:
    void decoded();
};



class ZBarFilterRunnable : public QVideoFilterRunnable
{

public:
    ZBarFilterRunnable(ZBarVideoFilter *filter);
    ~ZBarFilterRunnable();

    QVideoFrame run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags) Q_DECL_OVERRIDE;

private:
    ZBarVideoFilter *m_filter;
};
#endif // ZBARVIDEOFILTER_H
